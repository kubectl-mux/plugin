# `This repository is just a collection of ideas for now, no work done yet`

# kubectl-mux

The collection of tools and helpers for kubernetes in tmux environment under single umbrella kubectl plugin.

# Functions

 - [plan] kubectl mux: read config, run tmux initialization script
 - [plan] kubectl mux remote <url>: download and run on remote config (team sharing, helm chart based, etc)
 - [plan] kubectl mux logs: display logs in a several windows/panes
 - [plan] kubectl mux events: display events in a several windows/panes
 - [plan] kubectl mux ti <pod> <command> - connect window/pane to specified pod
 
---[plan] -- alpha release, can try to add to https://github.com/kubernetes-sigs/krew-index

 - [plan] kubectl mux gen: generate tmux initialization script based on open windows/panes
 - [plan] kubectl mux status on/off: enable/disable (and save in config) the status of kubectl (context, namespace, cluster, user)
 - [plan] kubectl mux status context/namespace/cluster/user on/off - enable/disable each part of kubectl status
 - [plan] kubectl mux status add <..?> - extendable configuration to add more info to status (like f.e. total cluster CPU load, or certain pod memory, etc)
 
Each command should modify config file .kmux.yml (or ~/.kubectl-mux/config.yml), saving the new state of terminal to be able to start from (more or less) the same place.

# Scenarios

## In helm

As an author of (for example) helm chart bootstrapping postges database, I can 
1) prepare script for tmux to run psql shell inside of `master` pod, while also showing master and slave logs in another panes/windows
2) publish it in github/gitlab/whatever
3) include disclaimer in NOTES.txt in the form `For kubectl-mux users: run 'kubectl mux remote https://<where-config-for-this-chart-is>' to connect to database...`

## In any project under git

As an DevOps/developer I want to conveniently share tmux config specialized for my project in a git repository. 
To do that I 
1) commit .kmux.yml file in the root of git repo. 
2) describe in README.md, how after clone, other developers could run 'kubectl mux' to immediately 
achieve specific connections (in windows/panes) to predefined list of clusters or list of services, etc

## In day-to-day activities

As a developer, who works with several clusters/projects, I need sometimes to switch 
between several contexts (not necessary only kubectl context).
To do that I 
1) create several folders in my home directory, where I checkout each project
2) in these folders I keep different .kmux.yml files, which I can use by just changing current directory (before launching tmux)

Also if kmux was initialized from folder without .kmux.yml, default ~/.kmux.yml could be used as a default fall-back. 
Once I shutdown the machine, config still could be used later to reinitialize the same windows/panes.